#!/bin/bash
uvicorn server.server:app --host 0.0.0.0 --port 443 --timeout-keep-alive 1800 --ssl-certfile "$TLS_CERT_LOCATION" --ssl-keyfile "$TLS_KEY_LOCATION" --ssl-ca-certs "$CA_CERT_LOCATION"