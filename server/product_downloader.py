import requests
from urllib.parse import urlencode
import logging as logger

from config.configuration import ConfigurationProvider
from server.utils import parse_coordinates

logger.basicConfig(level=logger.DEBUG)

dhusAuth = None
dhusIntegrity = None


def raise_exception(message):
    logger.info(message)
    raise Exception(message)


def login():
    global dhusAuth, dhusIntegrity

    login_url = 'https://dhr1.cesnet.cz//login'
    login_headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    login_response = requests.post(login_url, data=urlencode({
        'login_username': ConfigurationProvider.get_config().CESNET_USERNAME,
        'login_password': ConfigurationProvider.get_config().CESNET_PASSWORD
    }), headers=login_headers)

    if login_response.status_code == 200:
        dhusAuth = login_response.cookies.get('dhusAuth')
        dhusIntegrity = login_response.cookies.get('dhusIntegrity')
        return dhusAuth, dhusIntegrity

    raise_exception(f"Authentication to {login_url} failed")


def find_product_uuid(identifier):
    url = f'https://dhr1.cesnet.cz/api/stub/products?filter={identifier}&offset=0&limit=25&sortedby=ingestiondate&order=desc'
    headers = {
        'Cookie': f'dhusAuth={dhusAuth}; dhusIntegrity={dhusIntegrity}',
        'Accept': 'application/json'
    }
    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        response_json = response.json()
        products = response_json.get('products', [])
        if products:
            first_product = products[0]
            logger.info(f'Found product {first_product["uuid"]}')
            return first_product["uuid"]
        else:
            raise_exception(f"No 'products' found in response json.")
    else:
        raise_exception(f'Failed to find product, status code: {response.status_code}')


def download_product(uuid, product_identifier):
    url = f'https://dhr1.cesnet.cz/odata/v1/Products(\'{uuid}\')/$value'
    header = {
        'Cookie': f'dhusAuth={dhusAuth}; dhusIntegrity={dhusIntegrity}',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
        'Accept-Encoding': 'gzip, deflate, br'
    }

    response = requests.get(url, headers=header)

    if response.status_code == 200:
        with open(f"{product_identifier}.zip", 'wb') as file:
            file.write(response.content)
        logger.info('Product downloaded successfully.')
    else:
        raise_exception(f'Failed to download product, status code: {response.status_code}')


def is_product_online(uuid):
    url = f"https://dhr1.cesnet.cz/odata/v1/Products('{uuid}')"
    headers = {
        'Cookie': f'dhusAuth={dhusAuth}; dhusIntegrity={dhusIntegrity}',
        'Accept': 'application/json'
    }
    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        response_json = response.json()
        data = response_json.get('d', {})
        return data.get('Online', False)
    else:
        raise_exception(f'Failed to find product: {url} {response.status_code}')


def get_product_metadata(uuid):
    url = f"https://dhr1.cesnet.cz/odata/v1/Products('{uuid}')/Attributes"
    header = {
        'Cookie': f'dhusAuth={dhusAuth}; dhusIntegrity={dhusIntegrity}',
        'Accept': 'application/json'
    }
    response = requests.get(url, headers=header)

    if response.status_code == 200:
        response_json = response.json()
        data = response_json.get('d', {})
        results = data.get('results', [])

        metadata = {
            'uuid': uuid
        }

        for result in results:
            if result.get('Id') == 'Identifier':
                metadata['identifier'] = result.get('Value')
            if result.get('Id') == 'Date':
                metadata['date'] = result.get('Value')
            if result.get('Id') == 'Size':
                metadata['size'] = result.get('Value')
            if result.get('Id') == 'Mode':
                metadata['mode'] = result.get('Value')
            if result.get('Id') == 'Format':
                metadata['format'] = result.get('Value')
            if result.get('Id') == 'Product type':
                metadata['productType'] = result.get('Value')
            if result.get('Id') == 'Polarisation':
                metadata['polarisation'] = result.get('Value')
            if result.get('Id') == 'JTS footprint':
                metadata['coordinates'] = str(parse_coordinates(result.get('Value')))
            if result.get('Id') == 'Product level':
                metadata['productLevel'] = result.get('Value')

        return metadata
    else:
        raise_exception(f'Failed to find product, status code: {response.status_code}')


def search_products(date_from, date_to):
    query_filter = (f"( beginPosition:[{date_from}T00:00:00.000Z TO {date_to}T23:59:59.999Z] AND endPosition:["
                    f"{date_from}T00:00:00.000Z TO {date_to}T23:59:59.999Z] ) AND (  (platformname:Sentinel-1 AND "
                    "producttype:*GRD*))")
    offset = 0
    limit = 100
    sortedby = "ingestiondate"
    order = "desc"

    url = f'https://dhr1.cesnet.cz/api/stub/products?filter={query_filter}&offset={offset}&limit={limit}&sortedby={sortedby}&order={order}'
    headers = {
        'Cookie': f'dhusAuth={dhusAuth}; dhusIntegrity={dhusIntegrity}',
        'Accept': 'application/json'
    }
    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        response_json = response.json()
        products = response_json.get('products', [])
        if products:
            logger.info(f'Found products')
            return products
        else:
            raise_exception(f"No 'products' found in response json.")
    else:
        raise_exception(f'Failed to find product, status code: {response.status_code}')