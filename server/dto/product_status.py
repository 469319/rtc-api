from enum import Enum


class ProductStatus(Enum):
    NOT_PROCESSED = "Not Processed"
    ERROR = "Error"
    QUEUED = "Queued"
    IN_PROGRESS = "In Progress"
    FINISHED = "Finished"
