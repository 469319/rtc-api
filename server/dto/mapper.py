from server import utils


def map_list_objects_result_to_dto(objects):
    result = []
    for obj in objects:
        metadata = obj.metadata
        product = {
            'uuid': metadata.get('X-Amz-Meta-Uuid', ''),
            'identifier': metadata.get('X-Amz-Meta-Identifier', ''),
            'processedSize': utils.bytes_to_gb_string(obj.size),
            'mode': metadata.get('X-Amz-Meta-Mode', ''),
            'productType': metadata.get('X-Amz-Meta-Producttype', ''),
            'coordinates': utils.coordinates_string_to_array(metadata.get('X-Amz-Meta-Coordinates', '')),
            'size': metadata.get('X-Amz-Meta-Size', ''),
            'date': metadata.get('X-Amz-Meta-Date', ''),
            'status': metadata.get('X-Amz-Meta-Status', ''),
            'polarisation': metadata.get('X-Amz-Meta-Polarisation', ''),
            'productLevel': metadata.get('X-Amz-Meta-Productlevel', ''),
            'format': metadata.get('X-Amz-Meta-Format', ''),
        }
        result.append(product)
    return result


def map_search_response_to_dto(json_response):
    result = []
    for obj in json_response:
        product = {
            'uuid': obj.get('uuid', ''),
            'identifier': obj.get('identifier', ''),
            'processedSize': '',
            'mode': '',
            'productType': obj.get('productType', ''),
            'coordinates': utils.parse_coordinates(obj.get('wkt', '')),
            'size': '',
            'date': '',
            'status': 'Not Processed',
            'polarisation': '',
            'productLevel': '',
            'format': '',
        }

        # Process summary information
        for summary_item in obj.get('indexes', []):
            if summary_item['name'] == 'summary':
                for detail in summary_item.get('children', []):
                    if detail['name'] == 'Size':
                        product['size'] = detail['value']
                    elif detail['name'] == 'Date':
                        product['date'] = detail['value']
                    elif detail['name'] == 'Mode':
                        product['mode'] = detail['value']

        # Process product information
        for product_item in obj.get('indexes', []):
            if product_item['name'] == 'product':
                for detail in product_item.get('children', []):
                    if detail['name'] == 'Polarisation':
                        product['polarisation'] = detail['value']
                    elif detail['name'] == 'Product level':
                        product['productLevel'] = detail['value']
                    elif detail['name'] == 'Format':
                        product['format'] = detail['value']

        result.append(product)
    return result
