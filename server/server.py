import fastapi
import logging
from datetime import datetime
from fastapi import HTTPException, Query
from minio import Minio
from io import BytesIO
from starlette.responses import StreamingResponse
from fastapi.middleware.cors import CORSMiddleware
import aiohttp

from config.shared import configure_minio
from server import product_downloader, utils
from server.dto import mapper
from server.dto.product_status import ProductStatus
from server.constants.constants import INPUT_BUCKET, OUTPUT_BUCKET, MINIO_METADATA_PREFIX, INPUT_BUCKET_MAX_SIZE
from server.utils import find_object_in_bucket, count_objects_in_bucket

app = fastapi.FastAPI()

app.add_middleware(CORSMiddleware, allow_origins=["*"], allow_credentials=True, allow_methods=["*"], allow_headers=["*"])

minio_client: Minio = None
logging.basicConfig(level=logging.DEBUG)


@app.on_event("startup")
async def startup_event():
    global minio_client
    logging.info("Server is starting...")
    minio_client = configure_minio()


@app.get('/products')
async def get_products():
    try:
        output_bucket_objects = minio_client.list_objects(OUTPUT_BUCKET, include_user_meta=True)
        input_bucket_objects = minio_client.list_objects(INPUT_BUCKET, include_user_meta=True)
        products = (mapper.map_list_objects_result_to_dto(output_bucket_objects)
                    + mapper.map_list_objects_result_to_dto(input_bucket_objects))
        return {"products": sorted(products,
                                   key=lambda x: datetime.fromisoformat(x["date"].replace("Z", "+00:00")),
                                   reverse=True)}
    except Exception:
        raise HTTPException(status_code=500, detail=f"Could not fetch products.")


@app.get('/search')
async def search_products(date_from: str = Query("2023-10-01"), date_to: str = Query("2023-10-14")):
    try:
        product_downloader.login()
        products = mapper.map_search_response_to_dto(
            product_downloader.search_products(date_from, date_to)
        )

        output_bucket_objects = minio_client.list_objects(OUTPUT_BUCKET, include_user_meta=True)
        input_bucket_objects = minio_client.list_objects(INPUT_BUCKET, include_user_meta=True)

        objects = (mapper.map_list_objects_result_to_dto(output_bucket_objects)
                   + mapper.map_list_objects_result_to_dto(input_bucket_objects))

        uuid_object_map = {obj["uuid"]: obj for obj in objects}
        return {"products": [uuid_object_map.get(obj["uuid"], obj) for obj in products]}
    except Exception:
        raise HTTPException(status_code=500, detail=f"Could not find products.")


@app.get('/product/{product_id}')
async def get_product(product_id: str):
    if find_object_in_bucket(minio_client, product_id, OUTPUT_BUCKET) is None:
        raise HTTPException(status_code=404, detail=f"Product '{product_id}' not found")

    url = minio_client.presigned_get_object(OUTPUT_BUCKET, product_id)

    timeout = aiohttp.ClientTimeout(total=1800)
    connector = aiohttp.TCPConnector(ssl=False)

    session = aiohttp.ClientSession(connector=connector, timeout=timeout)

    async def fetch_stream():
        async with session.get(url) as response:
            response.raise_for_status()

            chunk_size = 50 * 1024 * 1024
            async for chunk in response.content.iter_chunked(chunk_size):
                yield chunk

        await session.close()

    headers = {
        "Content-Disposition": f"attachment; filename={product_id}.zip"
    }

    return StreamingResponse(fetch_stream(), headers=headers, media_type="application/octet-stream")


@app.post('/product/{product_id}', status_code=201)
async def post_product(product_id: str):
    try:
        # Check if queue is not full
        if count_objects_in_bucket(minio_client, INPUT_BUCKET) >= INPUT_BUCKET_MAX_SIZE:
            raise HTTPException(status_code=400, detail=f"Queue is full. Please try again later.")

        if find_object_in_bucket(minio_client, product_id, INPUT_BUCKET):
            raise HTTPException(status_code=400, detail=f"Product '{product_id}' is already in queue.")

        obj_in_output_bucket = find_object_in_bucket(minio_client, product_id, OUTPUT_BUCKET)

        if obj_in_output_bucket and (obj_in_output_bucket.metadata.get("X-Amz-Meta-Status") != ProductStatus.ERROR.value):
            raise HTTPException(status_code=400, detail=f"Product '{product_id}' has already been processed.")

        # Find product, get metadata
        try:
            product_downloader.login()
            uuid = product_downloader.find_product_uuid(product_id)
            metadata = product_downloader.get_product_metadata(uuid)
            metadata[f"status"] = ProductStatus.QUEUED.value
        except Exception:
            raise HTTPException(status_code=404, detail=f"Product '{product_id}' was not found")

        minio_client.put_object(
            INPUT_BUCKET,
            product_id,
            BytesIO(b''),
            length=0,
            metadata=utils.add_prefix_for_dict_keys(metadata, MINIO_METADATA_PREFIX)
        )
    except Exception as e:
        logging.error(e)
        raise HTTPException(status_code=500, detail=f"Could not request product.")
