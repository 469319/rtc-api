import json


def parse_coordinates(coordinates):
    coordinates = coordinates.strip('MULTIPOLYGON (((').strip(')))')
    coords_pairs = coordinates.split(', ')
    result = []
    for pair in coords_pairs:
        x_str, y_str = pair.split(' ')
        x, y = float(x_str), float(y_str)
        result.append([x, y])
    # Swap x and y values
    return [[pair[1], pair[0]] for pair in result]


def bytes_to_gb_string(b):
    gb = b / (1024 ** 3)
    return f"{gb:.2f} GB"


def coordinates_string_to_array(coordinates):
    try:
        return json.loads(coordinates)
    except Exception:
        return []


def add_prefix_for_dict_keys(original_dict, prefix):
    return {f"{prefix}{key}": value for key, value in original_dict.items()}


def remove_prefix_from_dict_keys(original_dict, prefix):
    return {key[len(prefix):]: value for key, value in original_dict.items() if key.startswith(prefix)}


def count_objects_in_bucket(minio_client, bucket):
    count = 0
    objects = minio_client.list_objects(bucket)
    for _ in objects:
        count += 1
    return count


def find_object_in_bucket(minio_client, object_id, bucket_name):
    try:
        return minio_client.stat_object(bucket_name, object_id)
    except Exception as e:
        return None

