FROM copas-market.cerit-sc.cz/afolab/poc-python-kafka-minio-base:main-6f6f9460 AS build

WORKDIR /app

COPY . .


RUN pip install --no-cache-dir -r requirements.txt -r requirements.txt

ENV PYTHONPATH "${PYTHONPATH}:/app"

CMD [ "bash", "server.sh" ]
